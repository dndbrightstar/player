# Player Material

Reference page for players

## Quick Map

* [Table Rules](https://dndbrightstar.gitlab.io/player/home-rules/index.html)
  * [Action Economy](https://dndbrightstar.gitlab.io/player/home-rules/action_economy.html)
  * [Death And Resurrection](https://dndbrightstar.gitlab.io/player/home-rules/death_resurrection.html)
  * [Death Saves](https://dndbrightstar.gitlab.io/player/home-rules/death_saves.html)
  * [Flanking](https://dndbrightstar.gitlab.io/player/home-rules/flanking.html)
  * [Lingering Injuries](https://dndbrightstar.gitlab.io/player/home-rules/lingering_injuries.html)
  * [Resting](https://dndbrightstar.gitlab.io/player/home-rules/rest.html)
  * [Stats And Leveling](https://dndbrightstar.gitlab.io/player/home-rules/stats.html)
* [Character Options](https://dndbrightstar.gitlab.io/player/characters/index.html)
  * [Backgrounds](https://dndbrightstar.gitlab.io/player/characters/backgrounds/index.html)
  * [Classes](https://dndbrightstar.gitlab.io/player/characters/classes/index.html)
  * [Feats](https://dndbrightstar.gitlab.io/player/characters/feats/index.html)
  * [Races](https://dndbrightstar.gitlab.io/player/characters/races/index.html)
  * [Spells](https://dndbrightstar.gitlab.io/player/characters/spells/index.html)

## Contributing

This page is made specificly for players at my table, so no external help is expected or needed. I also do not recommend using this page at your table directly as it will adapt to the needs of my table without warnings.
